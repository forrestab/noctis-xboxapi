﻿using Noctis.Integration.XboxApi.Models;
using Noctis.Integration.XboxApi.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi
{
    public partial class XboxApiSession
    {
        public Task<GameDetails> GetGameDetails(string productId)
        {
            Guard.AgainstNullArgument<string>(nameof(productId), productId);

            return this.GetAsync<GameDetails>($"game-details/{productId}");
        }
    }
}
