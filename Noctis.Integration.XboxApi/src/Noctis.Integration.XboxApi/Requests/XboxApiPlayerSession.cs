﻿using Noctis.Integration.XboxApi.Models;
using Noctis.Integration.XboxApi.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi
{
    public partial class XboxApiSession
    {
        public Task<long> GetXuid(string gamertag)
        {
            Guard.AgainstNullArgument<string>(nameof(gamertag), gamertag);

            return this.GetAsync<long>($"xuid/{gamertag}");
        }

        public Task<Profile> GetProfile(long xuid)
        {
            return this.GetAsync<Profile>($"{xuid}/profile");
        }

        public Task<Feed<PlayerEntry>> GetPlayerActivity(long xuid)
        {
            return this.GetPlayerActivity($"{xuid}/activity");
        }

        public Task<Feed<PlayerEntry>> GetPlayerActivity(long xuid, long continuationToken)
        {
            return this.GetPlayerActivity($"{xuid}/activity?contToken={continuationToken}");
        }

        private Task<Feed<PlayerEntry>> GetPlayerActivity(string uri)
        {
            return this.GetAsync<Feed<PlayerEntry>>(uri);
        }
    }
}
