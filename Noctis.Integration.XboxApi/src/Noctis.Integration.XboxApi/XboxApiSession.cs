﻿using Noctis.Integration.XboxApi.Extensions;
using Noctis.Integration.XboxApi.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi
{
    public partial class XboxApiSession : IXboxApiSession
    {
        private readonly string baseUrl = "https://xboxapi.com/v2/";
        private readonly HttpClient http;

        public XboxApiSession(HttpClient http)
        {
            Guard.AgainstNullArgument<HttpClient>(nameof(http), http);

            this.http = http;
        }

        private async Task<T> GetAsync<T>(string uri)
        {
            HttpResponseMessage Response = await this.http.GetAsync($"{this.baseUrl}{uri}");
            string Content = await Response.Content.ReadAsStringAsync();

            if (!Response.IsSuccessStatusCode)
            {
                // TODO, handle unsuccessful
            }

            return await Content.ParseJson<T>();
        }
    }
}
