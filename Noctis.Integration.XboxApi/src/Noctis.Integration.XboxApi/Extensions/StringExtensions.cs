﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Extensions
{
    public static class StringExtensions
    {
        public static Task<T> ParseJson<T>(this string json)
        {
            return Task.Factory.StartNew(() => JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings()));
        }
    }
}
