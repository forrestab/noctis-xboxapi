﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class CommunityEntry
    {
        public DateTimeOffset StartTime { get; set; }
        public int SessionDurationInMinutes { get; set; }
        public string ContentImageUri { get; set; }
        public string BingId { get; set; }
        public string ContentTitle { get; set; }
        public string VuiDisplayName { get; set; }
        public string Platform { get; set; }
        public int TitleId { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Date { get; set; }
        public bool HasUgc { get; set; }
        public string ActivityItemType { get; set; }
        public string ContentType { get; set; }
        public string ShortDescription { get; set; }
        public string ItemText { get; set; }
        public string ItemImage { get; set; }
        public string ShareRoot { get; set; }
        public string FeedItemId { get; set; }
        public string ItemRoot { get; set; }
        public bool HasLiked { get; set; }
        public CommunityAuthor AuthorInfo { get; set; }
        public long UserXuid { get; set; }
    }
}
