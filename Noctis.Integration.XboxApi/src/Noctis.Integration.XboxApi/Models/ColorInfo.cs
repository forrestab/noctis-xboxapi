﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class ColorInfo
    {
        public string PrimaryColor { get; set; }
        public string SecondaryColor { get; set; }
        public int TertiaryColor { get; set; }
    }
}
