﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class Activity
    {
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset EndTime { get; set; }
        [JsonProperty("numShares")]
        public int NumberOfShares { get; set; }
        [JsonProperty("numLikes")]
        public int NumberOfLikes { get; set; }
        [JsonProperty("numComments")]
        public int NumberofComments { get; set; }
        public string UgcCaption { get; set; }
        public string AuthorType { get; set; }
        public string ActivityItemType { get; set; }
        public long UserXuid { get; set; }
        public DateTimeOffset Date { get; set; }
        public string ContentType { get; set; }
        public int TitleId { get; set; }
        public string Platform { get; set; }
        public string SandboxId { get; set; }
        public string UserKey { get; set; }
        public string Scid { get; set; }
    }
}
