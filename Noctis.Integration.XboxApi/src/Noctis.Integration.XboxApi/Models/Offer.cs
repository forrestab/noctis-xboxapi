﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class Offer
    {
        public List<string> AcceptablePaymentInstrumentTypes { get; set; }
        public string AvailabilityDescription { get; set; }
        public string AvailabilityTitle { get; set; }
        public string CurrencyCode { get; set; }
        public string DisplayListPrice { get; set; }
        public int DisplayPositionTag { get; set; }
        public string DisplayPrice { get; set; }
        public string DistributionType { get; set; }
        public bool IsPurchasable { get; set; }
        public double ListPrice { get; set; }
        public string OfferId { get; set; }
        public string PrerequisiteProductId { get; set; }
        public string PrerequisiteProductType { get; set; }
        public double Price { get; set; }
        public string PromotionalText { get; set; }
        public string ReducedTitle { get; set; }
        public string TaxTypeCode { get; set; }
    }
}
