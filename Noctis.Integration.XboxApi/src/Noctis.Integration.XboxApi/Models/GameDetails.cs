﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class GameDetails
    {
        public string ImpressionGuid { get; set; }
        public List<GameDetail> Items { get; set; }
    }
}
