﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class RatingDisclaimer
    {
        public string Text { get; set; }
    }
}
