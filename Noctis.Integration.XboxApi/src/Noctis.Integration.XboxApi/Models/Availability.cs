﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class Availability
    {
        public string AvailabilityId { get; set; }
        public string ContentId { get; set; }
        public List<Device> Devices { get; set; }
        public string LicensePolicyTicket { get; set; }
        public Offer OfferDisplayData { get; set; }
        public string SignedOffer { get; set; }
        public List<SubscriptionBenefit> SubscriptionBenefits { get; set; }
    }
}
