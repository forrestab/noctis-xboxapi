﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class RatingDescriptor
    {
        public string NonLocalizedDescriptor { get; set; }
        public int Id { get; set; }
    }
}
