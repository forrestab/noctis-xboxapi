﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class Feed<T>
    {
        [JsonProperty("numItems")]
        public int NumberOfItems { get; set; }
        public long PollingToken { get; set; }
        [JsonProperty("pollingIntervalSeconds")]
        public int PollingIntervalInSeconds { get; set; }
        [JsonProperty("contToken")]
        public long ContinuationToken { get; set; }
        [JsonProperty("activityItems")]
        public List<T> Entries { get; set; }
    }
}
