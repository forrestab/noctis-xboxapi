﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class Author
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string ImageUrl { get; set; }
        public string AuthorType { get; set; }
        public long Id { get; set; }
    }
}
