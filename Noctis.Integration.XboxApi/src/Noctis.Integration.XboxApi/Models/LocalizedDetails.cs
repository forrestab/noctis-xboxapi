﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class LocalizedDetails
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public List<RatingImage> RatingImages { get; set; }
    }
}
