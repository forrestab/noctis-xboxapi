﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class LegacyId
    {
        public string IdType { get; set; }
        public object Value { get; set; }
    }
}
