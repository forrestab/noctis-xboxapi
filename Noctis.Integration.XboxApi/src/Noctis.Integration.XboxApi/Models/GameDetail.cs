﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class GameDetail
    {
        public string MediaGroup { get; set; }
        public string MediaItemType { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReducedDescription { get; set; }
        public string ReducedName { get; set; }
        public DateTimeOffset ReleaseDate { get; set; }
        public long TitleId { get; set; }
        public string VuiDisplayName { get; set; }
        public List<Genre> Genres { get; set; }
        public string DeveloperName { get; set; }
        public List<Image> Images { get; set; }
        public string PublisherName { get; set; }
        public DateTimeOffset Updated { get; set; }
        public string ParentalRating { get; set; }
        public string ParentalRatingSystem { get; set; }
        public string SortName { get; set; }
        public List<Capability> Capabilities { get; set; }
        public int KValue { get; set; }
        public string KValueNamespace { get; set; }
        public long HexTitleId { get; set; }
        public long AllTimePlayCount { get; set; }
        public long SevenDaysPlayCount { get; set; }
        public long ThirtyDaysPlayCount { get; set; }
        public long AllTimeRatingCount { get; set; }
        public long SevenDaysRatingCount { get; set; }
        public double AllTimeAverageRating { get; set; }
        public double ThirtyDaysAverageRating { get; set; }
        public double SevenDaysAverageRating { get; set; }
        public List<LegacyId> LegacyIds { get; set; }
        public List<Availability> Availabilities { get; set; }
        public string ResourceAccess { get; set; }
        public bool IsRetail { get; set; }
        public string ManualUrl { get; set; }
        public List<ParentalRating> ParentalRatings { get; set; }
    }
}
