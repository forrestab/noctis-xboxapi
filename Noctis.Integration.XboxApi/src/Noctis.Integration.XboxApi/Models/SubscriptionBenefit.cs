﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class SubscriptionBenefit
    {
        public string Id { get; set; }
        public List<string> Benefits { get; set; }
    }
}
