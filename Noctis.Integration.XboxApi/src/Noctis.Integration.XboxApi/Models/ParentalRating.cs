﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class ParentalRating
    {
        public string RatingId { get; set; }
        public int LegacyRatingId { get; set; }
        public string Rating { get; set; }
        public string RatingSystem { get; set; }
        public int RatingMinimumAge { get; set; }
        public LocalizedDetails LocalizedDetails { get; set; }
        public List<RatingDescriptor> RatingDescriptors { get; set; }
        public List<RatingDisclaimer> RatingDisclaimers { get; set; }
    }
}
