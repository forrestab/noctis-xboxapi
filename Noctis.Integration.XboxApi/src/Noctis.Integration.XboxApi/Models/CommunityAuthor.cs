﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class CommunityAuthor : Author
    {
        public ColorInfo Color { get; set; }
        public int ShowAsAvatar { get; set; }
    }
}
