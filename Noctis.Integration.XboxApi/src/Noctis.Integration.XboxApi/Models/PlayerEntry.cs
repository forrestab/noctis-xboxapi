﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class PlayerEntry : CommunityEntry
    {
        public DateTimeOffset EndTime { get; set; }
        public Activity Activity { get; set; }        
        public string Gamertag { get; set; }
        public string RealName { get; set; }
        public string DisplayName { get; set; }
        public string UserImageUri { get; set; }
        public string UserImageUriMd { get; set; }
        public string UserImageUriXs { get; set; }
    }
}
