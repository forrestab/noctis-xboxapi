﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Models
{
    public class Profile
    {
        public long Id { get; set; }
        public long? HostId { get; set; }
        public string Gamertag { get; set; }
        public string GameDisplayName { get; set; }
        public string AppDisplayName { get; set; }
        public int Gamerscore { get; set; }
        public string GameDisplayPicRaw { get; set; }
        public string AppDisplayPicRaw { get; set; }
        public string AccountTier { get; set; }
        public string XboxOneRep { get; set; }
        public string PreferredColor { get; set; }
        public int TenureLevel { get; set; }
        public bool IsSponsoredUser { get; set; }
    }
}
