﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi
{
    public interface IXboxApiClient
    {
        IXboxApiSession CreateSession(string apiKey);
    }
}
