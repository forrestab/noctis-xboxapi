﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi
{
    public class XboxApiClient : IXboxApiClient, IDisposable
    {
        private HttpClient http;

        public IXboxApiSession CreateSession(string apiKey)
        {
            this.CreateHttpClient(apiKey);

            return new XboxApiSession(this.http);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void CreateHttpClient(string apiKey)
        {
            this.http = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            });
            this.http.DefaultRequestHeaders.Add("X-Auth", apiKey);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if(this.http != null)
                {
                    this.http.Dispose();
                }
            }
        }
    }
}
