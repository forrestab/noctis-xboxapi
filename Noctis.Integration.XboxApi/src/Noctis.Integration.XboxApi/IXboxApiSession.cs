﻿using Noctis.Integration.XboxApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi
{
    public interface IXboxApiSession
    {
        #region Player Endpoints

        Task<long> GetXuid(string gamertag);
        Task<Profile> GetProfile(long xuid);
        Task<Feed<PlayerEntry>> GetPlayerActivity(long xuid);
        Task<Feed<PlayerEntry>> GetPlayerActivity(long xuid, long continuationToken);

        #endregion

        #region Game Endpoint

        Task<GameDetails> GetGameDetails(string productId);

        #endregion
    }
}
