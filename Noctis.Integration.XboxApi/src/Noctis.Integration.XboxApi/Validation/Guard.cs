﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Noctis.Integration.XboxApi.Validation
{
    public static class Guard
    {
        public static void AgainstNullArgument<T>(string argumentName, T argument) where T : class
        {
            if (argument == null)
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} is null.", argumentName), argumentName);
            }
        }
    }
}
